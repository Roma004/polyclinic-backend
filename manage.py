# -*- coding: utf-8 -*-
import argparse
import sys

usage_text: str = 'Use "manage.py migrate" to migrate database\n'


class MyParser(argparse.ArgumentParser):
    def error(self, message) -> None:
        sys.exit()


my_parser: MyParser = MyParser()
my_parser.add_argument("pram")

args: argparse.Namespace = my_parser.parse_args()
if args.pram == 'migrate':
    from db import *  # noqa

    db.evolve()  # noqa

else:
    print(usage_text)
