from db import Doctor
from decorators.json_validate import check_params
from decorators.identify_user import identify_user


@check_params()
@identify_user
def get_info(data: dict):

    polyclinic = data["user"].polyclinic

    doctors: Doctor = Doctor.select().where(Doctor.polyclinic == polyclinic)

    return {
        "polyclinic": polyclinic.get_dict() if polyclinic else {},
        "doctors": [d.get_dict() for d in doctors]
    }
