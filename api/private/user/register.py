# from email.policy import default
from db import User
from app import bcrypt
from decorators.json_validate import check_params
from utils.jwt_utils import generate_jwt_tokens

from errors import error_email_exists


@check_params(
    properties=[
        "birthdate", "adress", "passport", "snils", "phone_number"
    ],
    required=[
        "firstname", "secondname", "thirdname", "email", "password"
    ]
)
def register(data: dict) -> dict:
    # оставлю тут чтобы не забыть, какие поля нужны
    password = data["password"]
    del data["password"]

    try:
        user = User.select(
            User.uuid, User.email  # , User.passport, User.snils, User.phone_number
        ).where(
            User.email == data["email"]
            # User.passport == data["passport"],
            # User.snils == data["snils"],
            # User.phone_number == data["phone_number"]
        ).get()

        raise error_email_exists

    except User.DoesNotExist:
        pass

    user = None

    print(data, password)

    try:
        user: User = User(
            **data,
            password=bcrypt.generate_password_hash(password).decode()
        )
    except Exception as e:
        print(e)

    print(user.email, user.password)

    user.save(force_insert=True)

    return generate_jwt_tokens(user=user)
