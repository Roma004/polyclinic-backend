from db import User
from db import Polyclinic
from decorators.identify_user import identify_user
from decorators.json_validate import check_params
from app import bcrypt
from errors import error_duplication_entry
from errors import error_polyclinic_not_exists
from peewee import IntegrityError


@check_params(
    properties=[
        "firstname", "secondname", "thirdname", "email", "password",
        "birthdate", "adress", "passport", "snils", "phone_number"
    ]
)
@identify_user
def update(data: dict) -> dict:
    user: User = data["user"]
    if "password" in data:
        data["password"] = bcrypt.generate_password_hash(data["password"]).decode()
    if "polyclinic_uuid" in data:
        try:
            data["polyclinic"] = Polyclinic.select(
                Polyclinic.uuid
            ).where(
                Polyclinic.uuid == data["polyclinic_uuid"]
            ).get()
            del data["polyclinic_uuid"]
        except Polyclinic.DoesNotExist:
            raise error_polyclinic_not_exists
    del data["user"]
    
    try:
        update = User.update(**data).where(User.uuid == user.uuid).execute()
    except IntegrityError as e:
        err = error_duplication_entry
        err["error"] = e.args[1]
        raise err

    return {}
