from db import UserAnalyzes
from db import User
from datetime import datetime
from db.construct.analyze import Analyze
from decorators.identify_user import identify_user
from decorators.json_validate import check_params


@check_params(
    properties=[
        "search", "is_active"
    ],

    required=[
        "page", "order_by"
    ]
)
@identify_user
def get_analyzes(data: dict):
    user: User = data["user"]

    pages = UserAnalyzes.select().where(UserAnalyzes.user == user).count()
    options = [UserAnalyzes.user == user]
    if "is_active" in data:
        options.append(
            UserAnalyzes.expiration_date < datetime.today()
            if data["is_active"]
            else UserAnalyzes.expiration_date >= datetime.today()
        )
    analyzes = UserAnalyzes.select().where(
        *options,
        UserAnalyzes.analysis.in_(
            Analyze.select(Analyze.uuid).where(Analyze.type % f"%{data.get('search', '')}%")
        )
    ).order_by(data["order_by"]).limit(20).offset(data["page"]*20)

    ListAnalyzes = []

    for analysis in analyzes:
        ListAnalyzes.append({
            "analysis": analysis.analysis.get_dict(),
            "date_of_issue": str(analysis.date_of_issue),
            "expiration_date": str(analysis.expiration_date)
        })

    return {
        "max_pages": pages,
        "rows": ListAnalyzes
    }
