from db import UserDirections
from db import User
from db.construct.direction import Direction
from decorators.identify_user import identify_user
from decorators.json_validate import check_params


@check_params(
    properties=["search"],

    required=[
        "page", "order_by"
    ]
)
@identify_user
def get_directions(data: dict):
    user: User = data["user"]

    # print(data)

    pages = UserDirections.select().where(UserDirections.user == user).count()
    directions = UserDirections.select().where(
        UserDirections.user == user,
        UserDirections.direction.in_(
            Direction.select(Direction.uuid).where(Direction.type % f"%{data.get('search', '')}%")
        )
    ).order_by(data["order_by"]).limit(20).offset(data["page"]*20)

    # print(directions)

    ListDirections = []
    for direction in directions:
        # print(type(direction.date_of_issue), str(direction.date_of_issue))
        ListDirections.append({
            "direction": direction.direction.get_dict(),
            "doctor": direction.doctor.get_dict(["uuid", "firstname", "secondname", "thirdname"]),
            "date_of_issue": str(direction.date_of_issue),
            "expiration_date": str(direction.expiration_date)
        })

    return {
        "max_pages": pages,
        "rows": ListDirections
    }
