from db import Doctor
from errors import error_doctor_not_exists
from decorators.json_validate import check_params


@check_params()
def get_info(data: dict):

    try:
        doctor: Doctor = Doctor.select().where(Doctor.uuid == data["uuid"]).get()
    except Doctor.DoesNotExist:
        raise error_doctor_not_exists

    return doctor.get_dict()
