import os
import importlib
from typing import Callable

from errors import error_wrong_category, error_wrong_method


class API:
    method_tree: dict = {}

    def __init__(self, api_name: str):
        root_path = os.path.dirname(os.path.abspath(__file__))

        self.method_tree = {
            category: {
                mtd: getattr(
                    importlib.import_module(f"api.{api_name}.{category}.{mtd}"),
                    mtd
                ) for method in os.listdir(f"{root_path}/{api_name}/{category}")
                if ".py" in method and (mtd := method.replace(".py", ""))
            } for category in os.listdir(f"{root_path}/{api_name}")
            if category != "__pycache__"
        }

    def process(self, category: str, method: str, data: dict):
        try:
            ctgs: dict = self.method_tree[category]
        except KeyError:
            raise error_wrong_category

        try:
            mtds: Callable = ctgs[method]
        except KeyError:
            raise error_wrong_method

        data_json = mtds(data)

        return {
            "category": category,
            "method": method,
            "status": "ok",
            "info": data_json
        }
