from typing import Union


class SchemaConstructor:
    # ------------------- main -------------------
    top_scheme = {
        "type": "object",
        "additionalProperties": False,
    }
    default_properties = {

    }
    uuid_schema = {"type": "string", "minLength": 36, "maLength": 36}
    name_schema = {"type": "string", "minLength": 2, "maxLength": 30}
    time_schema = {
        "type": "string",
        "pattern": r"^(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)((-(\d{2}):(\d{2}))?)$",
        "minLength": 8,
        "maxLength": 10
    }
    props_schema = {
        "page": {"type": "integer", "minimum": 0, "maximum": 100},
        "order_by": {"enum": ["name", "date_of_issue", "expiration_date"]},
        # default
        "firstname": name_schema,
        "secondname": name_schema,
        "thirdname": name_schema,
        "phone_number": {"type": "string", "minLength": 11, "maxLength": 11},
        "start_working_time": time_schema,
        "end_working_time": time_schema,
        "adress": {"type": "string", "maxLength": 100},
        # User
        "birthdate": {"type": "string", "pattern": r"^(\d{4})-(\d{2})-(\d{2})$", "minLength": 10, "maxLength": 10},
        "email": {"type": "string", "minLength": 5, "maxLength": 30},
        "passport": {"type": "string", "minLength": 10, "maxLength": 10},
        "snils": {"type": "string", "minLength": 11, "maxLength": 11},
        "password": {"type": "string", "minLength": 8, "maxLength": 60},
        # Doctor
        "break_time": time_schema,
        "cabinet_number": {"type": "string", "maxLength": 5},
        # Polyclinic
        "number": {"type": "string", "maxLength": 10},
        "site_url": {"type": "string", "maxLength": 60},
        "is_adult": {"type": "boolean"},
        # Direction
        "date_of_issue": {"type": "string", "pattern": r"^(\d{4})-(\d{2})-(\d{2})$", "minLength": 10, "maxLength": 10},
        "expiration_date": {
            "type": "string", "pattern": r"^(\d{4})-(\d{2})-(\d{2})$", "minLength": 10, "maxLength": 10
        },
        "is_active": {"type": "boolean"},
        "search": {"type": "string", "minLength": 0, "maxLength": 100}
    }


def get_schema(
        properties: list[Union[tuple[str, str], str]] = None,
        required: list[str] = None
) -> dict:
    res = {
        "properties": {**SchemaConstructor.default_properties},
        **SchemaConstructor.top_scheme,
        "required": []
    }

    properties = [] if not properties else properties
    required = [] if not required else required

    for p in properties:
        res["properties"][p] = SchemaConstructor.props_schema[p]

    for p in required:
        res["properties"][p] = SchemaConstructor.props_schema[p]
        res["required"].append(p)

    return res
