from db import User
from errors import error_wrong_username_password
from app import bcrypt


def login(data: dict) -> User:
    try:
        user: User = User.get(
            User.email == data["user_name"],
            User.password.is_null(False)
        )
    except Exception:
        raise error_wrong_username_password

    if not bcrypt.check_password_hash(user.password, data["password"]):
        raise error_wrong_username_password

    return user
