from errors.custom_errors import ErrException

error_500 = ErrException("500 Internal Server Error", 500, 500)
error_404 = ErrException("404 Not Found", 404, 404)
error_405 = ErrException("405 Method Not Allowed", 405, 405)

error_type = ErrException("__custom__", 4)


error_wrong_category = ErrException("Wrong category", 1001)
error_wrong_method = ErrException("Wrong method", 1002)
error_data = ErrException("Error JSON data", 1003)

error_wrong_username_password = ErrException("Email or password is invalid", 5)


error_user_not_exists = ErrException("User does not exist", 1100)
error_polyclinic_not_exists = ErrException("Polyclinic does not exist", 1101)
error_doctor_not_exists = ErrException("Doctor does not exist", 1102)

error_email_exists = ErrException("User with that email is already registered", 1103)
error_duplication_entry = ErrException("", 1104)
