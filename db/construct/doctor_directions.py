from db.db import BaseModel
from peewee import ForeignKeyField
from db.construct.doctor import Doctor
from db.construct.direction import Direction


class DoctorDirections(BaseModel):
    doctor = ForeignKeyField(Doctor, backref='directions')
    direction = ForeignKeyField(Direction, backref='doctors')
