from db.db import BaseModel
from peewee import CharField, TimeField, ForeignKeyField, DateField
from db.construct.polyclinic import Polyclinic
from db.construct.doctor_special import DoctorSpecial


class Doctor(BaseModel):
    firstname = CharField(max_length=30)
    secondname = CharField(max_length=30)
    thirdname = CharField(max_length=30)
    polyclinic = ForeignKeyField(Polyclinic, backref='doctors')
    specialization = ForeignKeyField(DoctorSpecial, backref='doctors')
    start_working_time = TimeField()
    end_working_time = TimeField()
    break_time = TimeField()
    # working_days = DateField()  # FIXME это не одна дата, а несколько
    cabinet_number = CharField(max_length=5)

    __fields__ = [
        "firstname", "secondname", "thirdname", "start_working_time",
        "end_working_time", "break_time", "cabinet_number"
    ]
    __fg_keys__ = ["polyclinic", "specialization"]
