from db.db import BaseModel
from peewee import CharField


class DoctorSpecial(BaseModel):
    name = CharField(max_length=30)

    __fields__ = ["name"]
