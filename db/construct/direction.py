from db.db import BaseModel
from peewee import CharField, ForeignKeyField


class Direction(BaseModel):
    type = CharField()

    __fields__ = ["type"]
