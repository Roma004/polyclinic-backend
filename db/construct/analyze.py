from db.db import BaseModel
from peewee import CharField


class Analyze(BaseModel):
    type = CharField()
    ...  # TODO additional fields

    __fields__ = ["type"]
