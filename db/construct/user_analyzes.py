from db.db import BaseModel
from peewee import ForeignKeyField, DateField
from db.construct.user import User
from db.construct.analyze import Analyze


class UserAnalyzes(BaseModel):
    user = ForeignKeyField(User, backref='analyzes')
    analysis = ForeignKeyField(Analyze, backref='users')
    date_of_issue = DateField()
    expiration_date = DateField()
