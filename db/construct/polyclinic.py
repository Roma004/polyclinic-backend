from db.db import BaseModel
from peewee import CharField, BooleanField, TimeField


class Polyclinic(BaseModel):
    number = CharField(max_length=10)
    adress = CharField()
    start_wokring_time = TimeField()
    end_working_time = TimeField()
    working_days = CharField(max_length=7)
    phone_number = CharField(max_length=11)
    site_url = CharField()
    is_adult = BooleanField()

    __fields__ = [
        "number", "adress", "start_wokring_time",
        "end_working_time", "working_days", "phone_number",
        "site_url", "is_adult",
    ]
