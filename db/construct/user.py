from db.db import BaseModel
from db.construct.polyclinic import Polyclinic
from peewee import (
    CharField,
    ForeignKeyField,
    DateField,
)


class User(BaseModel):
    firstname = CharField(max_length=30)
    secondname = CharField(max_length=30)
    thirdname = CharField(max_length=30)

    polyclinic = ForeignKeyField(Polyclinic, backref='users', null=True)

    # age = SmallIntegerField()
    # is_adult = BooleanField(default=0)
    birthdate = DateField(null=True)
    adress = CharField(null=True)

    email = CharField(max_length=70, unique=True)
    passport = CharField(max_length=10, unique=True, null=True)
    snils = CharField(max_length=11, unique=True, null=True,)
    phone_number = CharField(max_length=11, unique=True, null=True)

    password = CharField(max_length=60)

    __fields__ = [
        "firstname", "secondname", "thirdname", "birthdate",
        "passport", "snils", "adress", "phone_number",
        "email"
    ]
    __fg_keys__ = ["polyclinic"]
