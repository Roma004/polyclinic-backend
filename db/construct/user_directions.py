from db.db import BaseModel
from peewee import ForeignKeyField, DateField
from db.construct.user import User
from db.construct.direction import Direction
from db.construct.doctor import Doctor


class UserDirections(BaseModel):
    user = ForeignKeyField(User, backref='directions')
    direction = ForeignKeyField(Direction, backref='users')
    date_of_issue = DateField()
    expiration_date = DateField()
    doctor = ForeignKeyField(Doctor, backref='directions')
