from typing import Callable, Union
from jsonschema import validate
from jsonschema.exceptions import ValidationError
from json_schema.json_schema import get_schema
from errors import error_type


def check_params(
    properties: list[Union[tuple[str, str], str]] = None,
    required: list[str] = None
) -> Callable:
    """
    Декоратор для проверки схемы JSON
    """

    def check_params_dec(fn) -> Callable:
        def wrapped(data: dict) -> dict:
            try:
                validate(instance=data, schema=get_schema(properties, required))
                return fn(data)

            except ValidationError as error:
                custom_error = error_type
                custom_error["error"] = error.message
                raise custom_error

        return wrapped

    return check_params_dec
