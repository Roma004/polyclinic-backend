from flask_jwt_extended import jwt_required, get_jwt_identity
from db import User
from typing import Callable
from errors import error_user_not_exists


def identify_user(fn) -> Callable:
    @jwt_required()
    def wrapped(data: dict) -> dict:
        new_data = data
        identity = get_jwt_identity()

        try:
            new_data["user"] = User.select().where(User.uuid == identity["id"]).get()
        except User.DoesNotExist:
            raise error_user_not_exists

        return fn(new_data)

    return wrapped
